#Stolen wholesale from https://kb.objectrocket.com/postgresql/insert-json-data-into-postgresql-using-python-part-2-1248
import json
import os
from psycopg2 import connect, Error

def insert_statement(json_input: list, db_connection):
    """ Insert sql string statement into db 
    TODO: Tidy up executemany() statement"""

    #Get the column names (dictionary keys) from the first record in json
    record_list = json.loads(json_input)
    columns = [list(x.keys()) for x in record_list][0]

    insert_list = []
    for i in record_list:
        insert_list.append(list(i.values()))
    #Create db connection cursor object
    cur = db_connection.cursor()

    # only attempt to execute SQL if cursor is valid
    if cur != None:

        try:
            cur.executemany("""INSERT INTO json_data 
                (scheduled_datetime_depart, 
                actual_datetime_depart, 
                depart_delta, delay_bool, 
                cancelled_bool, 
                scheduled_datetime_arrive, 
                actual_datetime_arrive, 
                arrive_delta, 
                rid, 
                start_station, 
                end_station) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", insert_list)
            db_connection.commit()
        except (Exception, Error) as error:
            print("\nexecute_sql() error:", error)
            db_connection.rollback()

        # close the cursor
        cur.close()

def verify_table(table_name: str, db_connection) -> dict:
    """ Check if table exists in db. If not create it. 
    Inputs: db name and existing db connection object (which it leaves open at the end)"""
    #Create db connection cursor object
    cur = db_connection.cursor()
    result_dict = {"Existing": False, "Created": False}

    #Check if table exists, if cursor is valid
    if cur != None:
        cur.execute("select exists(select * from information_schema.tables where table_name=%s)", (table_name,))
        result_dict["Existing"] = cur.fetchone()[0]

        #Create new table if required
        if not result_dict["Existing"]:
            create_table = f"""
                CREATE TABLE {table_name} (
                    id SERIAL PRIMARY KEY,
                    scheduled_datetime_depart VARCHAR(255),
                    actual_datetime_depart VARCHAR(255),
                    depart_delta real,
                    delay_bool BOOLEAN,
                    cancelled_bool BOOLEAN,
                    scheduled_datetime_arrive VARCHAR(255),
                    actual_datetime_arrive VARCHAR(255),
                    arrive_delta real,
                    rid VARCHAR(255),
                    start_station VARCHAR(255),
                    end_station VARCHAR(255)
                )
            """
            try:
                cur.execute(create_table)
                db_connection.commit()
                #Got to be anything better than below?
                result_dict["Created"] = True
            except (Exception, psycopg2.DatabaseError) as error:
                #table_bool = False
                result_dict["Created"] = False
                db_connection.rollback()
                print(error)

        cur.close()
        return result_dict

def new_db_connection():
    """ Starts a postgressql connection using env vars:
    RDS_ENDPOINT, RDS_DB_NAME, RDS_PORT, RDS_MASTER_USER, RDS_MASTER_PASS """
    #Connect to postgressql db
    try:
        # declare a new PostgreSQL connection object
        conn = connect(
            dbname = os.environ.get("RDS_DB_NAME"),
            user = os.environ.get("RDS_MASTER_USER"),
            host = os.environ.get("RDS_ENDPOINT"),
            password = os.environ.get("RDS_MASTER_PASS"),
            # attempt to connect for 3 seconds then raise exception
            connect_timeout = 3
        )

    except (Exception, Error) as err:
        print ("\npsycopg2 connect error:", err)
        conn = None

    return conn

def get_last_start_datetime(table_name: str, db_connection) -> str:
    """ Query db for last scheduled_datetime_depart value """

    cur = db_connection.cursor()
    if cur != None:
        try:
            cur.execute(f"SELECT id, scheduled_datetime_depart FROM {table_name} ORDER BY id DESC LIMIT 1;")
            result = cur.fetchone()[1]
            return result
        except (Exception, psycopg2.DatabaseError) as error:
            db_connection.rollback()
            print(error)

def get_depart_datetime_limit(table_name: str, limit: str, db_connection) -> str:
    """ Query db for first or last scheduled_datetime_depart value """

    if limit == 'max':
        limit_sql = "DESC "
    elif limit == 'min':
        limit_sql = ""
    else:
        raise ValueError("limit parameter: Acceptable values are 'min' or 'max'")

    cur = db_connection.cursor()
    if cur != None:
        try:
            cur.execute(f"SELECT scheduled_datetime_depart FROM {table_name} ORDER BY scheduled_datetime_depart {limit_sql}LIMIT 1;")
            result = cur.fetchone()[0]
            return result
        except (Exception, psycopg2.DatabaseError) as error:
            db_connection.rollback()
            print(error)
        cur.close()

def get_graph_data(table_name: str, db_connection) -> list:
    """ Query db for all scheduled_datetime_depart, arrive_delta, delay_bool, cancelled_bool values
        Returns a list of tuples, where each tuple is a db table row
        Assumes existing db connection """
    
    cur = db_connection.cursor()
    if cur != None:
        try:
            #Run the query to retrieve data
            cur.execute(f"select scheduled_datetime_depart, arrive_delta, delay_bool, cancelled_bool from {table_name} order by scheduled_datetime_depart;")
            rows = cur.fetchall()
        except (Exception, Error) as error:
            print("\nexecute_sql() error:", error)
            conn.rollback()
        cur.close()
    
    return rows