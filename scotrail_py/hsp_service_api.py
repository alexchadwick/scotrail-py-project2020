import requests 
import json
import os
from datetime import datetime, timedelta
from operator import itemgetter
from urllib3.util.retry import Retry

class RailService:
    """Model of train service using Nationalrail Darwin API"""

    def __init__(self, location_dict, start_station, end_station, date_str):
        self.start_station = start_station
        self.end_station = end_station
        self.location = location_dict
        self.date_str = date_str
        
        self.relative = {
            "start_station" : {
                "scheduled": "gbtt_ptd",
                "actual": "actual_td",
                "scheduled_datetime": "scheduled_datetime_depart",
                "actual_datetime": "actual_datetime_depart",
                "delta": "depart_delta"
            },
            "end_station" : {
                "scheduled": "gbtt_pta",
                "actual": "actual_ta",
                "scheduled_datetime": "scheduled_datetime_arrive",
                "actual_datetime": "actual_datetime_arrive",
                "delta": "arrive_delta"
            }
        }

        self.results = {}
    
    def location_details(self):
        """ Get all arrive or depart specific attributes of location"""

        #Select the correct dict of relative key values
        if self.location["location"] == self.start_station:
            relative_dict = self.relative["start_station"]
        elif self.location["location"] == self.end_station:
            relative_dict = self.relative["end_station"]
        else:
            return None

        #Build datetime string for scheduled arrival/departure
        self.results[relative_dict["scheduled_datetime"]] = self.date_str + "-" + self.location[relative_dict["scheduled"]]
        

        #If the train departed from/arrived at start/end station continue
        if self.location[relative_dict["actual"]] != '':
            #Build datetime string for actual arrival/departure
            self.results[relative_dict["actual_datetime"]] = self.date_str + "-" + self.location[relative_dict["actual"]]

            #Datetime object from strings
            sch_dt = datetime.strptime(self.results[relative_dict["scheduled_datetime"]], "%Y-%m-%d-%H%M")
            actual_dt = datetime.strptime(self.results[relative_dict["actual_datetime"]], "%Y-%m-%d-%H%M")
            #Train departed early or crossed into the next day (massive negative time difference)
            if actual_dt < sch_dt:
                self.results[relative_dict["delta"]] = -1 * (sch_dt - actual_dt).seconds /60
                #Check if likely to have crossed into the next day
                if self.results[relative_dict["delta"]] < -720:
                    actual_dt += timedelta(days=1)
                    self.results[relative_dict["actual_datetime"]] = actual_dt.strftime("%Y-%m-%d-%H%M")
                    self.results[relative_dict["delta"]] = (actual_dt - sch_dt).seconds /60
            else:
                self.results[relative_dict["delta"]] = (actual_dt - sch_dt).seconds /60
            #Check if delayed, where delta = no. mins late
            self.results["delay_bool"] = self.results[relative_dict["delta"]] > 0
            self.results["cancelled_bool"] = False
        else:
            self.results[relative_dict["actual_datetime"]] = None
            self.results[relative_dict["delta"]] = None
            self.results["delay_bool"] = False
            self.results["cancelled_bool"] = True

        return self.results
        
def post_service(service, pload):
    """HTTP POST call on HSP api against 'serviceMetrics' or 'serviceDetails' with dict input"""

    #Check arg input
    if isinstance(pload, dict) == False or service not in ['serviceDetails', 'serviceMetrics']:
        raise Exception("Param input error")

    #Two services to choose from: serviceDetails or serviceMetrics
    url = f'https://hsp-prod.rockshore.net/api/v1/{service}'
    header = {'Content-Type': 'application/json'}

    #HSP API creds from env vars
    username = os.environ.get("HSP_USER")
    password =  os.environ.get("HSP_SECRET")

    #Retry if http response = 502 - common if server load high
    session = requests.Session()
    retries = Retry(total=5,
                    backoff_factor=0.5,
                    status_forcelist=[500, 502, 503, 504],
                    method_whitelist=frozenset(['GET', 'POST']))
    session.mount('https://', requests.adapters.HTTPAdapter(max_retries=retries, pool_maxsize=30))

    #Response incl. raised exceptions
    try:
        r = session.post(url, headers=header,  
                auth=requests.auth.HTTPBasicAuth(username, password),
                data=json.dumps(pload))
        r.raise_for_status()
    except requests.exceptions.HTTPError as http_err: 
        print (f"Http Error: {http_err}, reduce query timeframe(metrics)/no. of rids(details)") 
    except requests.exceptions.RetryError as retry_err:
        print (f"Retry Error: {retry_err}")

    return(r.json())

def get_details(all_metric_dict, start_station, end_station):
    """Accepts json response from serviceMetrics POST and returns corresponding serviceDetails of all trains"""

    #Store RIDs of trains
    delayed = []
    for service_dict in all_metric_dict['Services']:
        [delayed.append(r) for r in service_dict['serviceAttributesMetrics']['rids']]

    #Run RIDs through serviceDetails and store as list of tuples
    details_list = []
    for rid in delayed:
        detail_dict = post_service("serviceDetails", {"rid": rid})
        loc_list = detail_dict["serviceAttributesDetails"]["locations"]
        date_str = detail_dict["serviceAttributesDetails"]["date_of_service"]
        result_dict = {}
        for loc in loc_list:
            loc_details = RailService(loc, start_station, end_station,date_str)
            if loc_details.location_details() != None:
                result_dict.update(loc_details.location_details())
        output_dict = {
            "rid": rid,
            "start_station": start_station,
            "end_station": end_station,
        }
        result_dict.update(output_dict)
        details_list.append(result_dict)

    #Need to sort by scheduled_datetime_depart
    return sorted(details_list, key=itemgetter('scheduled_datetime_depart'))

def split_metrics_post(start_station: str, end_station: str, start_datetime: str, end_datetime: str) -> list:
    """Create series of serviceMetrics POSTs with max 3hr time period from origin larger time period"""

    start = datetime.strptime(start_datetime, "%Y-%m-%d-%H%M")
    end = datetime.strptime(end_datetime, "%Y-%m-%d-%H%M")

    #Create a list of non-overlapping date time intervals which do not span dates
    datetime_list = []
    datetime_list.append(start)
    while start < end:
        start += timedelta(hours=6)
        if start < end:
            if start.date() > datetime_list[-1].date():
                start = datetime_list[-1].replace(hour=23, minute=59)
            datetime_list.append(start)
            start += timedelta(seconds=60)
            datetime_list.append(start)
        else:
            datetime_list.append(end)
            break

    #Build list of POST json
    pload_list = []
    #Step by 2 here to effectively group list of time intervals
    for n in range(0, len(datetime_list) - 1, 2):
        start_time = datetime.strftime(datetime_list[n], "%H%M")
        start_date = datetime.strftime(datetime_list[n], "%Y-%m-%d")
        end_time = datetime.strftime(datetime_list[n+1], "%H%M")
        # SHOULD BE THE SAME AS START DATE!
        end_date = datetime.strftime(datetime_list[n+1], "%Y-%m-%d")
        
        #Get day as either: WEEKDAY, SATURDAY or SUNDAY (before we increment time)
        return_day = ''
        weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]
        day_name = datetime.strftime(datetime_list[n], "%A")
        
        if day_name in weekdays:
            return_day = "WEEKDAY"
        else:
            return_day = day_name.upper()
        
        pload_template = {"from_loc": start_station,
        "to_loc":end_station,
        "from_time": start_time,     
        "to_time": end_time,
        "from_date": start_date,
        "to_date": end_date,
        "days": return_day}
        
        pload_list.append(pload_template)

    return pload_list