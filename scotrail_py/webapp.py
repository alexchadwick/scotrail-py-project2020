import plotly.graph_objects as go
from plotly.subplots import make_subplots
import os
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from datetime import datetime
import pandas as pd
import logging

from scotrail_py import psql_db

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

#Connect to db and check for existing table
table_name = "json_data"
conn = psql_db.new_db_connection()
table_exists = psql_db.verify_table(table_name, conn)

#Pull data from postgres
rows = psql_db.get_graph_data(table_name, conn)
delayed_rows = [ r[0:2] for r in rows if r[1] != None if r[1] > 0 ]
cancelled_rows = [ (r[0]) for r in rows if r[3] ]
arrive_delta = [ r[1] for r in rows if r[2] ]

#Transpose and unpack delayed_rows list
transposed_delay_list = [list(i) for i in zip(*delayed_rows)]
scheduled_datetime_depart_delay, arrive_delta_delay = transposed_delay_list
scheduled_datetime_depart_delay_dto = [datetime.strptime(s, "%Y-%m-%d-%H%M") for s in scheduled_datetime_depart_delay]

#Calc freq of cancellations 
df = pd.DataFrame({"date":cancelled_rows, "count":len(cancelled_rows)*[1]})
df.date = pd.to_datetime(df.date)
dg = df.groupby(pd.Grouper(key='date', freq='1M')).sum() # groupby each 1 month
dg.index = dg.index.strftime('%B')
#xvalue = [ i for i in dg.index.values[0:5] ]
yvalue = [ y for i in dg.values for y in i ]

#Start Dash, w/ support for gunicorn
app = dash.Dash()
server = app.server
colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}

#Plot results
layout1 = go.Layout(title = 'Journey Delays between BDG and GLC', hovermode = 'closest')
fig = make_subplots(
    rows=3, 
    cols=1, 
    shared_xaxes=False,
    subplot_titles=("Arrival Delay (mins)",
    "No. cancelled per month",
    "5 fig summary of arrival delays"),
    vertical_spacing=0.1
    )
fig.add_trace(
    go.Scattergl(x=scheduled_datetime_depart_delay_dto, y=arrive_delta_delay, mode='markers', name='delay_delta'),
    row=1, col=1
    )
fig.add_trace(
    go.Bar(x=dg.index, y=yvalue, name='cancelled'),
    row=2, col=1
    )
fig.add_trace(
    go.Box(y=arrive_delta, boxpoints='all', name='delay_5fig'),
    row=3, col=1
    )
fig.update_layout(height=850,
    plot_bgcolor=colors['background'],
    paper_bgcolor=colors['background'],
    font_color=colors['text']
) 

#Dash layout
app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.H1(
        children='Dashboard',
        style={
            'textAlign': 'left',
            'color': colors['text'],
            'padding' : '8px'
        }
    ),

    html.Div(children='Rail Delays and Cancellations between BDG and GLC', style={
        'textAlign': 'left',
        'color': colors['text'],
        'padding' : '8px'
    }),

    dcc.Graph(id = 'plot', figure = fig)
    ])

if __name__ == '__main__':
    app.run_server(debug = False)