#!/usr/bin/python3

import json
import os
import sys
import getopt
import logging
from datetime import datetime, timedelta
from psycopg2 import connect, Error

from scotrail_py import hsp_service_api
from scotrail_py import psql_db

def main(argv):
    #params
    start_station = "BDG"
    end_station = "GLC"
    table_name = "json_data"
    log_level = ''
    input_start_datetime = None
    input_end_datetime = None
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

    #Set start and end datetimes using env vars, if they are set
    env_start = os.environ.get("HSP_START_DATETIME")
    env_end = os.environ.get("HSP_END_DATETIME")
    if env_start:
        input_start_datetime = env_start
    if env_end:
        input_end_datetime = env_end

    #Override env vars with parameter input
    try:
        opts, args = getopt.getopt(argv, "hs:e:",["start=","end="])
    except getopt.GetoptError:
        print("scotrail.py -s <start_datetime> -e <end_datetime>")
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("scotrail.py -s <start_datetime> -e <end_datetime>")
            sys.exit()
        elif opt in ("-s", "start="):
            input_start_datetime = arg
        elif opt in ("-e", "end="):
            input_end_datetime = arg

    #Default time inputs
    if not input_start_datetime:
        input_start_datetime = datetime.strftime(datetime.now() - timedelta(days=14), "%Y-%m-%d-%H%M")
    if not input_end_datetime:
        input_end_datetime = datetime.strftime(datetime.now(), "%Y-%m-%d-%H%M")

    #Connect to db and check for existing table
    conn = psql_db.new_db_connection()
    table_exists = psql_db.verify_table(table_name, conn)
    logging.debug(f"DB table {table_name} exists: {table_exists}")

    #All ServiceMetrics resonses appended to this list, then fed into hsp_service_api.get_details
    response_list = []

    #Get required query start and end time to update any existing records so result includes input range, without duplicates
    if table_exists["Existing"]:
        logging.debug(f"Existing DB Table {table_name}. Comparing contents to new request...")
        #Check db for existing record "scheduled_datetime_depart" range. Assuming contiguous
        earliest_datetime = psql_db.get_depart_datetime_limit(table_name, "min", conn)
        latest_datetime = psql_db.get_depart_datetime_limit(table_name,"max", conn)

        #If input start date < existing lower limit, then grab input start date to existing lower limit
        if datetime.strptime(input_start_datetime, "%Y-%m-%d-%H%M") < datetime.strptime(earliest_datetime, "%Y-%m-%d-%H%M"):
            logging.debug(f"New request demands content preceading oldest db record - {earliest_datetime}. Updating...")
            start_datetime = input_start_datetime
            #Need to decrement end_datetime by 1 min to avoid duplicate records (avoids existing lower limit)
            end_datetime = datetime.strptime(earliest_datetime, "%Y-%m-%d-%H%M")
            end_datetime_decremented = datetime.strftime(end_datetime + timedelta(seconds=-60), "%Y-%m-%d-%H%M")

            #Create list of suitable POST json
            logging.debug(f"Creating pload list between {start_datetime} and {end_datetime_decremented}")
            pload_list = hsp_service_api.split_metrics_post(start_station, end_station, start_datetime, end_datetime_decremented)
            #POST against serviceMetrics to return rids
            for pload in pload_list:
                response = hsp_service_api.post_service("serviceMetrics", pload)
                #If no trains ran during the time window of this response, throw it away
                if len(response['Services']) != 0:
                    response_list.append(response)
                else:
                    logging.debug("POST successful but response empty. No services in this period.")
        
        #If input end date > existing upper limit, then grab existing upper limit to end input date
        if datetime.strptime(latest_datetime, "%Y-%m-%d-%H%M") < datetime.strptime(input_end_datetime, "%Y-%m-%d-%H%M"):
            logging.debug(f"New request demands content newer than newest db record - {latest_datetime}. Updating...")
            #Need to increment end_datetime by 1 min to avoid duplicate records
            start_datetime = datetime.strptime(latest_datetime, "%Y-%m-%d-%H%M")
            start_datetime_incremented = datetime.strftime(start_datetime + timedelta(seconds=60), "%Y-%m-%d-%H%M")
            end_datetime = input_end_datetime

            #Create list of suitable POST json
            logging.debug(f"Creating pload list between {start_datetime_incremented} and {end_datetime}")
            pload_list = hsp_service_api.split_metrics_post(start_station, end_station, start_datetime_incremented, end_datetime)
            #POST against serviceMetrics to return rids
            for pload in pload_list:
                response = hsp_service_api.post_service("serviceMetrics", pload)
                #If no trains ran during the time window of this response, throw it away
                if len(response['Services']) != 0:
                    response_list.append(response)
                else:
                    logging.debug("POST successful but response empty. No services in this period.")
    
    elif not table_exists["Existing"] and table_exists["Created"]:
        logging.debug(f"No existing DB Table {table_name}. Created new db table and processing request...")
        start_datetime = input_start_datetime
        end_datetime = input_end_datetime

        #Create list of suitable POST json
        logging.debug(f"Creating pload list between {start_datetime} and {end_datetime}")
        pload_list = hsp_service_api.split_metrics_post(start_station, end_station, start_datetime, end_datetime)
        #POST against serviceMetrics to return rids
        for pload in pload_list:
            response = hsp_service_api.post_service("serviceMetrics", pload)
            #If no trains ran during the time window of this response, throw it away
            if len(response['Services']) != 0:
                response_list.append(response)
            else:
                logging.debug("POST successful but response empty. No services in this period.")

    #Only attempt ServiceDetails req if received valid ServiceMetrics response
    if response_list:
        sorted_details_list = []
        for response in response_list:
            if len(response['Services']) != 0:
                sorted_details = hsp_service_api.get_details(response, start_station, end_station)
                sorted_details_list.extend(sorted_details)

        #Test new psql_db module
        sorted_details_list_dump = json.dumps(sorted_details_list)
        #sql_string = psql_db.create_insert_string(sorted_details_list_dump, table_name)
        psql_db.insert_statement(sorted_details_list_dump, conn)
        result = psql_db.get_last_start_datetime(table_name, conn)
        print(f"Last scheduled_datetine_depart: {result}")
    else:
        logging.debug("No ServiceMetrics in time period. No ServiceDetails requests attempted")
    # close the cursor and connection
    conn.close()
    logging.debug("Closing DB connection")

if __name__ == "__main__":
   main(sys.argv[1:])