""" Schedules 'working hours' for ECS task 'web' to reduce costs """

import boto3
import os

#Replace with env vars, linked to ssm params in lambda definition
cluster_name = os.environ.get("cluster_name")
service_name = os.environ.get("service_name")
desired_task_number = os.environ.get("desired_task_number")

def lambda_handler(event, context):
    schedule_ecs_service()

def schedule_ecs_service():
    """ Start/stop ecs service by setting the desired number of tasks to 1/0 """

    client = boto3.client('ecs')
    response = client.update_service(
        cluster=cluster_name,
        service=service_name,
        desiredCount=int(desired_task_number)
    )
    return response