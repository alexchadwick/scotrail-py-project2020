""" Restarts task "web" container in response to trigger. Requires a Cloudwatch Lambda Subscription filter, with filter pattern: "DEBUG - Closing DB connection" """

import boto3
import os

#Replace with env vars, linked to ssm params in lambda definition
cluster_name = os.environ.get("cluster_name")
service_name = os.environ.get("service_name")

def lambda_handler(event, context):
    force_new_ecs_task_deployment()

def force_new_ecs_task_deployment():
    """ Force Service Task to redeploy. Hoping this will gracefully update the web task """

    client = boto3.client('ecs')
    response = client.update_service(
        cluster=cluster_name,
        service=service_name,
        forceNewDeployment=True
    )
    return response