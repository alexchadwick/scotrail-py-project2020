import hsp_service_api
import json

#Set start and end stations using ROC
start_station = "BDG"
end_station = "GLC"

#Used downloaded json metrics output
with open('data/metrics_BDG_GLC_2020-02-11-0700_2020-02-11-0800.json') as f:
    all_metric_dict = json.load(f)

sorted_details_list = hsp_service_api.get_details(all_metric_dict, start_station, end_station)

with open('data/get_details_example.json', 'w') as f:
    json.dump(sorted_details_list, f, indent=4)