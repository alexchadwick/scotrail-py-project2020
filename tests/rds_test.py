#Stolen wholesale from https://kb.objectrocket.com/postgresql/insert-json-data-into-postgresql-using-python-part-2-1248
import json
import sys
import os
from psycopg2 import connect, Error

with open('data/details_2020-09-07-0000_2020-09-07-2359.json') as json_data:
    #Using load(), cf loads(). To convert a JSON-compliant string into Python objects use the json.loads() method instead. The “s” at the end of loads stands for “string”
    #Might need to use loads() in actual function, if we don't export to file?
    record_list = json.load(json_data)

if type(record_list) == list:
    first_record = record_list[0]

    # get the column names from the first record
    columns = list(first_record.keys())
    #Equivalent
    #columns = [list(x.keys()) for x in record_list][0]
    print ("\ncolumn names:", columns)

#Declare the string for SQL statement
table_name = "json_data"
sql_string = 'INSERT INTO {} '.format( table_name )
sql_string += "(" + ', '.join(columns) + ")\nVALUES "

sql_string += "('2020-09-01-1246', NULL, True, False, NULL, '2020-09-01-1251', NULL, NULL, '202009018914822', 'BDG', 'GLC')  "

#Build the rest of the SQL complient string from json object
# enumerate over the record
#for i, record_dict in enumerate(record_list):
#
#    # iterate over the values of each record dict object
#    values = []
#    for col_names, val in record_dict.items():
#
#        # Postgres strings must be enclosed with single quotes
#        if type(val) == str:
#            # escape apostrophies with two single quotations
#            val = val.replace("'", "''")
#            val = "'" + val + "'"
#
#        values += [ str(val) ]
#    # join the list of values and enclose record in parenthesis
#    sql_string += "(" + ', '.join(values) + "),\n"

# remove the last comma and end statement with a semicolon
sql_string = sql_string[:-2] + ";"

#Print resultant SQL string
print ("\nSQL statement:")
print (sql_string)

#Connect to postgressql db
try:
    # declare a new PostgreSQL connection object
    conn = connect(
        dbname = os.environ.get("RDS_DB_NAME"),
        user = os.environ.get("RDS_MASTER_USER"),
        host = os.environ.get("RDS_ENDPOINT"),
        password = os.environ.get("RDS_MASTER_PASS"),
        # attempt to connect for 3 seconds then raise exception
        connect_timeout = 3
    )

    cur = conn.cursor()
    print ("\ncreated cursor object:", cur)

except (Exception, Error) as err:
    print ("\npsycopg2 connect error:", err)
    conn = None
    cur = None

#Check if table exists
cur.execute("select exists(select * from information_schema.tables where table_name=%s)", (table_name,))
table_bool = cur.fetchone()[0]

#Create new table if required
if not table_bool:
    create_table = f"""
        CREATE TABLE {table_name} (
            id SERIAL PRIMARY KEY,
            scheduled_datetime_depart VARCHAR(255),
            actual_datetime_depart VARCHAR(255),
            depart_delta real,
            delay_bool BOOLEAN,
            cancelled_bool BOOLEAN,
            scheduled_datetime_arrive VARCHAR(255),
            actual_datetime_arrive VARCHAR(255),
            arrive_delta real,
            rid VARCHAR(255),
            start_station VARCHAR(255),
            end_station VARCHAR(255)
        )
    """
    try:
        cur.execute(create_table)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    #Leave connection open

string = 'INSERT INTO json_data (scheduled_datetime_depart, actual_datetime_depart, depart_delta, delay_bool, cancelled_bool, scheduled_datetime_arrive, actual_datetime_arrive, arrive_delta, rid, start_station, end_station) VALUES %s'

list = [('2020-09-01-1235', '2020-09-01-1234', -1.0, False, False, '2020-09-01-1239', '2020-09-01-1239', 0.0, '202009018916188', 'BDG', 'GLC'),
    ('2020-09-01-1246', None, None, False, True, '2020-09-01-1251', None, None, '202009018914822', 'BDG', 'GLC')]

# only attempt to execute SQL if cursor is valid
if cur != None:

    try:
        cur.executemany("""INSERT INTO json_data 
            (scheduled_datetime_depart, 
            actual_datetime_depart, 
            depart_delta, delay_bool, 
            cancelled_bool, 
            scheduled_datetime_arrive, 
            actual_datetime_arrive, 
            arrive_delta, 
            rid, 
            start_station, 
            end_station) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""", list)
        conn.commit()

        print ('\nfinished INSERT INTO execution')

    except (Exception, Error) as error:
        print("\nexecute_sql() error:", error)
        conn.rollback()

    # close the cursor and connection
    cur.close()
    conn.close()