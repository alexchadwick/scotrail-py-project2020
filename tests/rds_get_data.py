import os
from psycopg2 import connect, Error

table_name = "json_data"

#Connect to postgressql db
try:
    # declare a new PostgreSQL connection object
    conn = connect(
        dbname = os.environ.get("RDS_DB_NAME"),
        user = os.environ.get("RDS_MASTER_USER"),
        host = os.environ.get("RDS_ENDPOINT"),
        password = os.environ.get("RDS_MASTER_PASS"),
        # attempt to connect for 3 seconds then raise exception
        connect_timeout = 3
    )

    cur = conn.cursor()
    print ("\ncreated cursor object:", cur)

except (Exception, Error) as err:
    print ("\npsycopg2 connect error:", err)
    conn = None
    cur = None

#Check if table exists
cur.execute("select exists(select * from information_schema.tables where table_name=%s)", (table_name,))
table_bool = cur.fetchone()[0]

if table_bool:
    # only attempt to execute SQL if cursor is valid
    if cur != None:
        try:
            #Run the query to retrieve data
            cur.execute(f"select scheduled_datetime_depart, arrive_delta, depart_delta from {table_name} order by scheduled_datetime_depart;")
            rows = cur.fetchall()
        except (Exception, Error) as error:
            print("\nexecute_sql() error:", error)
            conn.rollback()

#Transpose (convert from list of row values to list of column values) list of tuples w/ zip()
transposed_list = [list(i) for i in zip(*rows)]
#unpack list
scheduled_datetime_depart, arrive_delta, depart_delta = transposed_list

# close the cursor and connection
cur.close()
conn.close()