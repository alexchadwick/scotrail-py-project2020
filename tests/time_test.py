from datetime import datetime, timedelta

start_time = datetime(2020, 9, 9, 20, 00)
end_time = datetime(2020, 9, 10, 5, 00)

time_list = []
time_list.append(start_time)
while start_time < end_time:
    start_time += timedelta(hours=3)
    if start_time < end_time:
        if start_time.date() > time_list[-1].date():
            start_time = time_list[-1].replace(hour=23, minute=59)
        time_list.append(start_time)
        start_time += timedelta(seconds=60)
        time_list.append(start_time)
    else:
        time_list.append(end_time)
        break

for n in range(0, len(time_list) - 1, 2):
        start_time = time_list[n]
        end_time = time_list [n+1]
