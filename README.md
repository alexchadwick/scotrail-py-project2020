**Launch**

gunicorn scotrail_py.webapp:server -b :\<port-no\>

OR

docker run projectpenfold/scotrailpy

See https://hub.docker.com/repository/docker/projectpenfold/scotrailpy/general

**Intro**

Train RID = YYYYmmdd<UID-ascii-char><UID-5-int>, e.g. 202002107124712, 2020-02-10, UID=G24712

[Scotrail delay repay table](https://www.scotrail.co.uk/plan-your-journey/our-delay-repay-guarantee)

|Ticket Type| 30 to 59 minutes | 1 hour to 1 hour 59 minutes |2 hours or more |
| ------ | ------ | ------ | ------ |
|Single| 50% of the cost | The full cost  |The full cost  |
|Return| 25% of the cost | 50% of the cost|The full cost  | 
|Season week | ticket cost ÷ 20 | ticket cost ÷ 10 |ticket cost ÷ 5|
|Season month | ticket cost ÷ 80| ticket cost ÷ 40 |ticket cost ÷ 20|

**Aim**

- Get list of all trains between 2 stations, during date range, as list of RIDs
- Check depart_delay and arrive_delay for each train

- End Figures:
    - Total no of delays per day for multiple arrive_delta thresholds
    - Arrival delays per time/train-UID
    - Calc monetary value of delay repay, incl. 15-30 range which scotrail doesn't currently support

**Env vars**   
    HSP_USER: Username for basic auth against HSP API @ https://hsp-prod.rockshore.net/  
    HSP_SECRET: Password for basic auth against HSP API @ https://hsp-prod.rockshore.net/


**Cloudformation Deployment**

*Initial Manual Steps*
Create secrets:
    "gitlab-container-registry-read" - dictionary, {"username": "value", "password": "value"}
    "HspSecret" - string, "value"

Create s3 buckets for:
    - cfn templates - param "TemplateBucketURL", in every top level cfn stack template
    - lambda packages - cfn/lambda-stack.yml, param "LambdaS3Bucket"