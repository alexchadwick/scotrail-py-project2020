---

Parameters:
  StackName:
    Type: String
  VPCCIDR:
    Type: AWS::SSM::Parameter::Value<String>
    Default: /scotrailpy/app/VPCCIDR
  PrivateSubnetACIDR:
    Type: AWS::SSM::Parameter::Value<String>
    Default: /scotrailpy/app/PrivateSubnetACIDR
  PrivateSubnetBCIDR:
    Type: AWS::SSM::Parameter::Value<String>
    Default: /scotrailpy/app/PrivateSubnetBCIDR
  PublicSubnetACIDR:
    Type: AWS::SSM::Parameter::Value<String>
    Default: /scotrailpy/app/PublicSubnetACIDR
  PublicSubnetBCIDR:
    Type: AWS::SSM::Parameter::Value<String>
    Default: /scotrailpy/app/PublicSubnetBCIDR

Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties: 
      CidrBlock: !Ref VPCCIDR
      EnableDnsHostnames: True
      EnableDnsSupport: True
      InstanceTenancy: default
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-vpc'

  InternetGateway:
    Type: AWS::EC2::InternetGateway
    DependsOn: VPC
    Properties:
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-InternetGateway'
  AttachInternetGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC

  PublicSubnetA:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select [ 0, !GetAZs ]
      CidrBlock: !Ref PublicSubnetACIDR
      MapPublicIpOnLaunch: True
      VpcId: !Ref VPC
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-public-subnet-a'

  PublicSubnetB:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select [ 1, !GetAZs ]
      CidrBlock: !Ref PublicSubnetBCIDR
      MapPublicIpOnLaunch: True
      VpcId: !Ref VPC
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-public-subnet-b'

  PrivateSubnetA:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select [ 0, !GetAZs ]
      CidrBlock: !Ref PrivateSubnetACIDR
      MapPublicIpOnLaunch: False
      VpcId: !Ref VPC
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-private-subnet-a'

  PrivateSubnetB:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select [ 1, !GetAZs ]
      CidrBlock: !Ref PrivateSubnetBCIDR
      MapPublicIpOnLaunch: False
      VpcId: !Ref VPC
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-private-subnet-b'

  NATGateway:
    Type: AWS::EC2::NatGateway
    Properties: 
      AllocationId: !GetAtt NATGatewayEIP.AllocationId
      SubnetId: !Ref PublicSubnetA
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-nat-gateway'
  NATGatewayEIP:
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-nat-gateway-eip'

  PublicRouteTable:
    Type: AWS::EC2::RouteTable
    Properties: 
      VpcId: !Ref VPC
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-public-route-table'
  PublicRoute:
    Type: AWS::EC2::Route
    DependsOn: AttachInternetGateway
    Properties: 
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway
      RouteTableId: !Ref PublicRouteTable
  PublicRouteTableAssociationA:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties: 
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnetA
  PublicRouteTableAssociationB:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties: 
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnetB

  PrivateRouteTable:
    Type: AWS::EC2::RouteTable
    Properties: 
      VpcId: !Ref VPC
      Tags:
        - Key: Project
          Value: !Ref StackName
        - Key: Name
          Value: !Sub '${StackName}-private-route-table'
  PrivateRoute:
    Type: AWS::EC2::Route
    Properties: 
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NATGateway
      RouteTableId: !Ref PrivateRouteTable
  PrivateRouteTableAssociationA:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties: 
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnetA
  PrivateRouteTableAssociationB:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties: 
      RouteTableId: !Ref PrivateRouteTable
      SubnetId: !Ref PrivateSubnetB

  PrivateSubnetAParam:
    Type: AWS::SSM::Parameter
    Properties: 
      Name: /scotrailpy/app/PrivateSubnetA
      Tier: Standard
      Type: String
      Value: !Ref PrivateSubnetA
  PrivateSubnetBParam:
    Type: AWS::SSM::Parameter
    Properties: 
      Name: /scotrailpy/app/PrivateSubnetB
      Tier: Standard
      Type: String
      Value: !Ref PrivateSubnetB
  PublicSubnetAParam:
    Type: AWS::SSM::Parameter
    Properties: 
      Name: /scotrailpy/app/PublicSubnetA
      Tier: Standard
      Type: String
      Value: !Ref PublicSubnetA
  PublicSubnetBParam:
    Type: AWS::SSM::Parameter
    Properties: 
      Name: /scotrailpy/app/PublicSubnetB
      Tier: Standard
      Type: String
      Value: !Ref PublicSubnetB
  VPCParam:
    Type: AWS::SSM::Parameter
    Properties: 
      Description: Main VPC for scotrailpy app project
      Name: /scotrailpy/app/VPC
      Tier: Standard
      Type: String
      Value: !Ref VPC

Outputs:
  PrivateSubnetA:
    Value: !Ref PrivateSubnetA
  PrivateSubnetB:
    Value: !Ref PrivateSubnetB
  PublicSubnetA:
    Value: !Ref PublicSubnetA
  PublicSubnetB:
    Value: !Ref PublicSubnetB
  VPC:
    Value: !Ref VPC