# Create or update cfn stack "lambda-stack"
name: $(Date:yyyyMMdd)$(Rev:.r)

trigger: none
schedules:
- cron: "0 20 * * *"
  displayName: Daily delete all stacks at 2000 UTC
  branches:
    include:
      - release

pool:
  vmImage: ubuntu-latest

variables:
  - template: /pipelines/cfn/vars.yml

steps:
  - task: AWSShellScript@1
    displayName: Check Lambda Stack
    inputs:
      awsCredentials: ${{ variables.awsServiceConnection }}
      regionName: ${{ variables.awsDefaultRegion }}
      scriptType: 'inline'
      inlineScript: |
        LAMBDA_STACK=$(aws cloudformation list-stacks \
          --stack-status-filter CREATE_COMPLETE UPDATE_COMPLETE ROLLBACK_COMPLETE UPDATE_ROLLBACK_COMPLETE \
          --query "StackSummaries[].[StackName] | [?contains(@, '${{ variables.rootStackName }}-lambda')]" --output text)
        echo "##vso[task.setvariable variable=LAMBDA_STACK;]${LAMBDA_STACK}"
      failOnStandardError: true

  - task: CloudFormationDeleteStack@1
    displayName: Delete Lambda Stack
    inputs:
      awsCredentials: ${{ variables.awsServiceConnection }}
      regionName: ${{ variables.awsDefaultRegion }}
      stackName: "${{ variables.rootStackName }}-lambda"
      logRequest: true
      logResponse: true
    condition: eq(variables['LAMBDA_STACK'], '${{ variables.rootStackName }}-lambda') #NB. variables['ENV_STACK'] is required syntax to expand a runtime variable in conditional

  - task: AWSShellScript@1
    displayName: Check Fargate Stack
    inputs:
      awsCredentials: ${{ variables.awsServiceConnection }}
      regionName: ${{ variables.awsDefaultRegion }}
      scriptType: 'inline'
      inlineScript: |
        FARGATE_STACK=$(aws cloudformation list-stacks \
          --stack-status-filter CREATE_COMPLETE UPDATE_COMPLETE ROLLBACK_COMPLETE UPDATE_ROLLBACK_COMPLETE \
          --query "StackSummaries[].[StackName] | [?contains(@, '${{ variables.rootStackName }}-fargate')]" --output text)
        echo "##vso[task.setvariable variable=FARGATE_STACK;]${FARGATE_STACK}"
      failOnStandardError: true

  - task: CloudFormationDeleteStack@1
    displayName: Delete Fargate Stack
    inputs:
      awsCredentials: ${{ variables.awsServiceConnection }}
      regionName: ${{ variables.awsDefaultRegion }}
      stackName: "${{ variables.rootStackName }}-fargate"
      logRequest: true
      logResponse: true
    condition: eq(variables['FARGATE_STACK'], '${{ variables.rootStackName }}-fargate')

  - task: AWSShellScript@1
    displayName: Check Shared Stack
    inputs:
      awsCredentials: ${{ variables.awsServiceConnection }}
      regionName: ${{ variables.awsDefaultRegion }}
      scriptType: 'inline'
      inlineScript: |
        SHARED_STACK=$(aws cloudformation list-stacks \
          --stack-status-filter CREATE_COMPLETE UPDATE_COMPLETE ROLLBACK_COMPLETE UPDATE_ROLLBACK_COMPLETE \
          --query "StackSummaries[].[StackName] | [?contains(@, '${{ variables.rootStackName }}-shared')]" --output text)
        echo "##vso[task.setvariable variable=SHARED_STACK;]${SHARED_STACK}"
      failOnStandardError: true

  - task: CloudFormationDeleteStack@1
    displayName: Delete Shared Stack
    inputs:
      awsCredentials: ${{ variables.awsServiceConnection }}
      regionName: ${{ variables.awsDefaultRegion }}
      stackName: "${{ variables.rootStackName }}-shared"
      logRequest: true
      logResponse: true
    condition: eq(variables['SHARED_STACK'], '${{ variables.rootStackName }}-shared')

  - task: AWSShellScript@1
    displayName: Check Env Stack
    inputs:
      awsCredentials: ${{ variables.awsServiceConnection }}
      regionName: ${{ variables.awsDefaultRegion }}
      scriptType: 'inline'
      inlineScript: |
        ENV_STACK=$(aws cloudformation list-stacks \
          --stack-status-filter CREATE_COMPLETE UPDATE_COMPLETE ROLLBACK_COMPLETE UPDATE_ROLLBACK_COMPLETE \
          --query "StackSummaries[].[StackName] | [?contains(@, '${{ variables.rootStackName }}-env')]" --output text)
        echo "##vso[task.setvariable variable=ENV_STACK;]${ENV_STACK}"
      failOnStandardError: true

  - task: CloudFormationDeleteStack@1
    displayName: Delete Env Stack
    inputs:
      awsCredentials: ${{ variables.awsServiceConnection }}
      regionName: ${{ variables.awsDefaultRegion }}
      stackName: "${{ variables.rootStackName }}-env"
      logRequest: true
      logResponse: true
    condition: eq(variables['ENV_STACK'], '${{ variables.rootStackName }}-env')